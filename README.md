# Worktopus

Simple multi-thread parallel processing with dynamic JavaScript Web Workers.

[Get started](https://worktopus.jsweb.app/)
