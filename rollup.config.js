import pkg from './package.json'
import babel from 'rollup-plugin-babel'

const author = `/**
 * @author Alex Bruno Cáceres
 * @email git.alexbr@outlook.com
 * @date 2017-03-01 09:27:58
 * @desc Simple multi-thread parallel processing with dynamic JavaScript Web Workers
 */`

export default [{
  input: 'src/browser.js',
  plugins: [babel()],
  output: {
    format: 'umd',
    name: pkg.name.replace('@jsweb/', ''),
    file: pkg.browser,
    banner: author
  }
}, {
  input: 'src/node.js',
  external: ['os', 'tiny-worker'],
  plugins: [babel()],
  output: {
    format: 'cjs',
    name: pkg.name,
    file: pkg.main,
    banner: author
  }
}]
