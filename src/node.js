import Worktopus from './module'
import Worker from 'tiny-worker'
import { cpus } from 'os'

export default function(data, options) {
  return new Worktopus(data, options, cpus().length || 4, Worker, true)
}
