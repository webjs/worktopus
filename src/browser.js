import Worktopus from './module'

export default function(data, options) {
  return new Worktopus(data, options, navigator.hardwareConcurrency || 4, Worker, false)
}
