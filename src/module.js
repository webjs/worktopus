// Privates
const privates = {}

function Private(key) {
  if (!privates[key])
    privates[key] = Symbol(key)
  return privates[key]
}

export default class Worktopus {
  /**
   * Creates Worktopus instance
   *
   * @param {any} data
   * @param {object} options
   * @param {number} threads
   * @param {any} worker
   * @param {boolean} node
   *
   * @memberOf Worktopus
   */
  constructor(data, options, threads, worker, node) {
    this.data = data
    this.options = Object.assign({
      max: threads - 1,
      env: {},
    }, options)

    // Private props

    this[Private('worker')] = worker
    this[Private('node')] = node

    this[Private('flow')] = Promise.resolve(data)

    this[Private('code')] = null
    this[Private('blob')] = null

    this[Private('functions')] = []
    this[Private('scripts')] = []
    this[Private('unpkg')] = []
  }

  /**
   * Set a Worker enviroment variable attached to 'self.env'
   *
   * @param {String} key
   * @param {any} value
   * @returns {Worktopus}
   *
   * @memberOf Worktopus
   */
  env(key, value) {
    this.options.env[key] = value
    return this
  }

  /**
   * Import code to use at Worker enviroment like functions or scripts
   * Accepts any number of mixed arguments like scripts path strings,
   * named functions or literal objects
   *
   * @param {any} args
   * @returns {Worktopus}
   *
   * @memberOf Worktopus
   */
  imports(...args) {
    args.forEach(arg => {
      switch (typeof arg) {
        case 'string':
          this[Private('scripts')].push(`'${arg}'`)
          break
        case 'function':
          this[Private('functions')].push({
            name: arg.name,
            code: arg
          })
          break
        default:
          this[Private('functions')].push(arg)
          break
      }
    })
    return this
  }

  /**
   * Import NPM modules from UNPKG CDN to use at Worker enviroment
   * Accepts any number of string arguments to identify modules
   *
   * @param {any} args
   * @returns
   *
   * @memberOf Worktopus
   */
  packages(...args) {
    this[Private('unpkg')] = args.map(path => `'https://unpkg.com/${path}'`)
    return this
  }

  /**
   * Executes a task with a single parallel thread
   *
   * @param {Function} task(data) {}
   * @returns Worktopus
   *
   * @memberOf Worktopus
   */
  exec(task) {
    if (!this.data)
      return this

    this[Private('flow')] = this[Private('flow')].then(() => {
      const fn = task.toString(),
        wk = this[Private('setWorker')](fn)

      return new Promise(done => {
        wk.postMessage(this.data)
        wk.addEventListener('message', e => {
          this[Private('shutdown')]([wk])
          this.data = e.data
          done(this.data)
        })
      })
    })

    return this
  }

  /**
   * Executes a multi-thread Array.map method
   *
   * @param {Function} task(item, index, array) {}
   * @returns Worktopus
   *
   * @memberOf Worktopus
   */
  map(task) {
    if (!this.data)
      return this

    this[Private('flow')] = this[Private('flow')].then(() => {
      let total = parseInt(this.data.length),
        thread = 0,
        proxy = []

      const exec = 'task(o.v, o.k)',
        result = '{ v: r, k: o.k }',
        job = `o => { const task = ${task}, r = ${exec}; return ${result} }`,
        cluster = this[Private('setCluster')](job),
        loop = (v, k) => {
          if (thread === cluster.length)
            thread = 0

          return cluster[thread++].postMessage({
            v,
            k
          })
        },
        back = done => {
          this[Private('shutdown')](cluster)
          this.data = proxy
          return done(this.data)
        }

      return new Promise(done => {
        function ping(e) {
          proxy[e.data.k] = e.data.v
          return --total || back(done)
        }
        cluster.forEach(wk => wk.addEventListener('message', ping))
        this.data.forEach((v, k) => this[Private('asap')](loop, v, k))
      })
    })
    return this
  }

  /**
   * Executes a multi-thread Array.reduce method
   *
   * @param {Function} task(result, item, index, array) {}
   * @param {any} result
   * @returns Worktopus
   *
   * @memberOf Worktopus
   */
  reduce(task, arg) {
    if (!this.data)
      return this

    this[Private('flow')] = this[Private('flow')].then(() => {
      let total = 0,
        thread = 0,
        limit = 100,
        parts = [],
        proxy = [],
        cluster = []

      const job = `o => o.a.reduce(${task}, o.r)`,
        last = this[Private('setWorker')](job),
        finish = all => {
          if (Array.isArray(all[0]))
            all = all.reduce((a, i) => a.concat(i), [])
          last.postMessage({
            a: all,
            r: arg
          })
        },
        loop = part => {
          if (thread === cluster.length)
            thread = 0
          return cluster[thread++].postMessage({
            a: part,
            r: arg
          })
        },
        back = (done, result) => {
          this[Private('shutdown')](cluster.concat(last))
          this.data = result
          return done(this.data)
        },
        ping = e => {
          proxy.push(e.data)
          return --total || finish(proxy)
        }

      return new Promise(done => {
        last.addEventListener('message', e => back(done, e.data))

        if (this.data.length <= limit)
          return finish(this.data)

        cluster = this[Private('setCluster')](job)
        cluster.forEach(wk => wk.addEventListener('message', ping))

        while (data.length)
          parts.push(this.data.splice(0, limit))

        total = parseInt(parts.length)
        return parts.forEach(part => this[Private('asap')](loop, part))
      })
    })

    return this
  }

  /**
   * Executes a single parallel thread Array.filter method
   *
   * @param {Function} task(item) {}
   * @returns Worktopus
   *
   * @memberOf Worktopus
   */
  filter(task) {
    const job = `a => a.filter(${task})`
    return this.exec(job)
  }

  /**
   * Executes a single parallel thread Array.sort method
   *
   * @param {Function} task(a, b) {}
   * @returns Worktopus
   *
   * @memberOf Worktopus
   */
  sort(task) {
    const job = `a => a.sort(${task})`
    return this.exec(job)
  }

  /**
   * Async step Promise-like then
   *
   * @param {Function} task(data) {}
   * @returns Worktopus
   *
   * @memberOf Worktopus
   */
  then(task) {
    this[Private('flow')] = this[Private('flow')].then(() => task(this.data))
    return this
  }

  /**
   * Async step Promise-like then, but finishes all instance operations
   *
   * @param {Function} task(data) {}
   * @returns {Function} task
   *
   * @memberOf Worktopus
   */
  finally(task) {
    this[Private('flow')].then(() => {
      task(this.data)
      delete this.data
    })
  }

  // Private methods

  [Private('asap')](fn, ...args) {
    return this[Private('node')] ? setImmediate.call(this, fn, ...args) : setTimeout(fn, 1, ...args)
  }

  [Private('getWorker')](str) {
    const worker = ['"use strict"'],
      env = JSON.stringify(this.options.env),
      scripts = this[Private('scripts')].join(','),
      unpack = this[Private('unpkg')].join(','),
      footer = [
        `const env = ${env}, job = ${str};`,
        'self.addEventListener("message", e => {',
        'const result = job(e.data);',
        'self.postMessage(result);',
        '})'
      ].join(' ')

    if (scripts.length)
      worker.push(`importScripts(${scripts})`)

    if (unpack.length)
      worker.push(`importScripts(${unpack})`)

    this[Private('functions')].forEach(fn => {
      const func = fn.name ? `const ${fn.name} = ${fn.code}` : fn.code.toString()
      worker.push(func)
    })

    this[Private('code')] = worker.concat(footer).join('\n')
    return this[Private('code')]
  }

  [Private('getBlobUrl')](wk) {
    const blob = new Blob([wk], {
      type: 'application/javascript'
    })
    this[Private('blob')] = URL.createObjectURL(blob)
    return this[Private('blob')]
  }

  [Private('setWorker')](str) {
    const code = this[Private('code')] || this[Private('getWorker')](str),
      wk = this[Private('node')] ? new Function(code) : this[Private('blob')] || this[Private('getBlobUrl')](code)
    return new this[Private('worker')](wk)
  }

  [Private('setCluster')](task) {
    const cluster = []

    for (let i = parseInt(this.options.max); i > 0; i--)
      cluster.push(this[Private('setWorker')](task))

    return cluster
  }

  [Private('shutdown')](cluster) {
    this[Private('code')] = null
    cluster.forEach(wk => wk.terminate())
    if (this[Private('blob')]) {
      URL.revokeObjectURL(this[Private('blob')])
      this[Private('blob')] = null
    }
  }
}
