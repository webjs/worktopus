import pkg from '../../package.json'

// Global state
export function state() {
  return {
    name: pkg.name,
    lang: null,
    menu: []
  }
}

export const mutations = {
  set(state, value) {
    Object.assign(state, value)
  }
}

export const getters = {
  menu(s) {
    return s.menu.map(item => {
      item.link = item.route.replace(':lang', s.lang)
      return item
    })
  }
}

export const actions = {
}
