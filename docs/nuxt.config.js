const pkg = require('../package')

module.exports = {
  mode: 'spa',

  router: {
    middleware: 'router'
  },

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/normalize.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/bulma/css/bulma.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/animate.css/animate.min.css'
      },
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff',
    height: '5px'
  },

  transition: {
    enterActiveClass: 'animated fadeIn'
  },

  /*
   ** Global CSS
   */
  css: [
    '@/assets/global.styl'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [],

  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
