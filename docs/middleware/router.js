export default function ({ isClient, store, route }) {
  if (isClient) {
    const { lang } = route.params
    const change = lang !== store.state.lang

    if (change) {
      store.commit('set', { lang })

      if (lang) {
        fetch(`/data/${lang}.json`)
          .then(resp => resp.json())
          .then(data => {
            store.commit('set', { menu: data.menu })
          })
      } else {
        store.commit('set', { menu: [] })
      }
    }
  }
}
